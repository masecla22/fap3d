#include <iostream>
#include "fap3d.h"
#include <windows.h>
#include <vector>
#include <ctime>
#include <random>

using namespace fap3d::setup;
using namespace fap3d::shapes::bidimensional;
using namespace fap3d::colors;
using namespace fap3d;
int corns = 400;
double degtorad(double rads){
    return rads*180/fap3d::globals::PI;
}
int random(int min, int max)
{
    if(min==max)
        return min;
    static bool first = true;
    if ( first )
    {
        srand(time(NULL));
        first = false;
    }
    return min + rand() % (max - min);
}
int main() {
    Window d = Window(0,0,900,600);
    d.open();
    Point a(200,200),b(300,300);
    int i=0,colts=5;
    while(true)
    {
        a.rotate(d.getCenter(),2*globals::PI/colts);
        b.rotate(d.getCenter(),2*globals::PI/(colts+1));
        Line(a,b).draw();
        i++;
        Sleep(100);
    }
    _getch();
    return 0;
}
